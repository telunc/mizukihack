//
//  AddViewController.m
//  Muzik_Hack_Demo
//
//  Created by andy on 2016-04-24.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "AddViewController.h"
#import "AddTableViewCell.h"

@interface AddViewController ()

@property (nonatomic, strong) NSMutableArray *peopleArray;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UINavigationBar appearance] setTranslucent:YES];
    [self setTitle:@"Nearby Friends"];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil]];
    // Do any additional setup after loading the view from its nib.
    
    //static array
    self.peopleArray = [[NSMutableArray alloc] init];
    
    [self.peopleArray addObject:@"Leroy Sanchez" ];
    [self.peopleArray addObject:@"Cam" ];
    [self.peopleArray addObject:@"Just" ];
    [self.peopleArray addObject:@"OMI" ];
    
    self.tableView.rowHeight = 80.0;
    
    //magic
    [self.tableView registerNib:[UINib nibWithNibName:@"AddTableViewCell" bundle:nil] forCellReuseIdentifier:@"AddTableViewCell"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.peopleArray.count;
}

- (UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    AddTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddTableViewCell" forIndexPath:indexPath];
    cell.name.text = [self.peopleArray objectAtIndex:[indexPath row]];
    NSString *string = @"photo";
    NSString *string2 = [@([indexPath row] +1) stringValue];
    NSLog(@"%ld", (long)[indexPath row]);
    NSLog(@" %s", self.user.hasLory ? "true" : "false");
    if ((int)[indexPath row]==0 && self.user.hasLory){
        [cell.switcher setOn:1];
    }
    else if (((int)[indexPath row]==0 && !self.user.hasLory)) {
        [cell.switcher setOn:0];
    }
    if ((int)[indexPath row]==1 && self.user.hasCam){
        [cell.switcher setOn:1];
    }
    else if ((int)[indexPath row]==1 && !self.user.hasCam){
        [cell.switcher setOn:0];
    }
    if ((int)[indexPath row]==2 && self.user.hasJustin){
        [cell.switcher setOn:1];
    }
    else if ((int)[indexPath row]==2 && !self.user.hasJustin){
        [cell.switcher setOn:0];
    }
    if ((int)[indexPath row]==3 && self.user.hasOMI){
        [cell.switcher setOn:1];
    }
    else if ((int)[indexPath row]==3 && !self.user.hasOMI){
        [cell.switcher setOn:0];
    }
    cell.profileImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", string, string2]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AddTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if ([cell.switcher isOn]){
        [cell.switcher setOn:0];
        if ((int)[indexPath row]==0){
            self.user.hasLory = [@0 boolValue];
        }
        if ((int)[indexPath row]==1){
            self.user.hasCam = [@0 boolValue];
        }
        if ((int)[indexPath row]==2){
            self.user.hasJustin = [@0 boolValue];
        }
        if ((int)[indexPath row]==3){
            self.user.hasOMI = [@0 boolValue];
        }
    }
    else{
        [cell.switcher setOn:1];
        if ((int)[indexPath row]==0){
            self.user.hasLory = [@1 boolValue];
        }
        if ((int)[indexPath row]==1){
            self.user.hasCam = [@1 boolValue];
        }
        if ((int)[indexPath row]==2){
            self.user.hasJustin = [@1 boolValue];
        }
        if ((int)[indexPath row]==3){
            self.user.hasOMI = [@1 boolValue];
        }

    }
}


-(void)add{
    AddViewController *add = [AddViewController alloc];
    add.user = self.user;
    [self.navigationController presentViewController:[add init] animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
