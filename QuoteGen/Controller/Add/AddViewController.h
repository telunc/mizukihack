//
//  AddViewController.h
//  Muzik_Hack_Demo
//
//  Created by andy on 2016-04-24.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface AddViewController : UIViewController

@property (nonatomic,strong) User* user;

@end
