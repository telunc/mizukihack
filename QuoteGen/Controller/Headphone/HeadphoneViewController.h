//
//  HeadphoneViewController.h
//  Muzik_Hack_Demo
//
//  Created by andy on 2016-04-23.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZAccessory.h"
#import "User.h"

@interface HeadphoneViewController : UIViewController
@property (nonatomic, strong) MZAccessory *mz;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property User* user;

@end
