//
//  LoginViewController.m
//  Muzik_Hack_Demo
//
//  Created by andy on 2016-04-23.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "LoginViewController.h"
#import "HeadphoneViewController.h"
#import "User.h"

@interface LoginViewController ()
@property (strong, nonatomic) IBOutlet UITextField *user;
@property (strong, nonatomic) IBOutlet UITextField *pwd;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)connect:(id)sender {
    HeadphoneViewController *view = [HeadphoneViewController alloc];
    
    //declare user
    User *user = [[User alloc] init];
    user.hasLory = [@1 boolValue];
    user.hasCam = [@1 boolValue];
    user.hasJustin = [@1 boolValue];
    user.hasOMI= [@1 boolValue];
    view.user = user;
    
    
    UINavigationController *view_nav = [[UINavigationController alloc] initWithRootViewController:[view init]];
    //[self.navigationController pushViewController: view_nav animated:YES];
    [self presentViewController:view_nav animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
