//
//  AddTableViewCell.h
//  Muzik_Hack_Demo
//
//  Created by andy on 2016-04-24.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UISwitch *switcher;

@end
