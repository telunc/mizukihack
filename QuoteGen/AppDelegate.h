//
//  AppDelegate.h
//  QuoteGen
//
//  Created by Alex on 2016-04-19.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

