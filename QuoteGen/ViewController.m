//
//  ViewController.m
//  QuoteGen
//
//  Created by Alex on 2016-04-19.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "ViewController.h"
#import "MZAccessory.h"
#import "MZGesture.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UITextField *inputField;
@property (nonatomic, weak) IBOutlet UILabel *battery;
@property (nonatomic, weak) IBOutlet UILabel *btlocalname;

@end

@implementation ViewController


- (IBAction)Battery{
    [self.mz getBatteryLevelWithBlock:^(NSInteger percent) {
        self.battery.text = [[NSString alloc] initWithFormat:@"%ld", (long)percent];
        //NSLog(@"%ld", (long) percent);
    }];
}

- (IBAction)BTLocalName {
    
    // Get the local blue tooth name
    [self.mz getBluetoothLocalNameWithBlock:^(NSString *name) {
        self.btlocalname.text = name;
    }];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mz = [[MZAccessory alloc] init];
    
    // Start the server
    [self.mz startServer];
    
    
    // Register for connection state
    [self.mz registerForConnectionState:^(NSUInteger state) {
        switch(state) {
            case 4:
                NSLog(@"headphones ARE connected");
                break;
            case 0:
                NSLog(@"headphones NOT connected");
                break;
            default:
                NSLog(@"something else");
        }
    }];
    
    [self registerGestures];
}

- (void)registerGestures{
    
    // Register a callback for the up button
    MZGesture* gesture = [MZGesture buttonUpGesture];
    NSArray *gestures = [NSArray arrayWithObjects: gesture, nil];
    [_mz registerForGestures: gestures  withBlock:^(MZGesture *gesture, Byte *data) {
        // The call back logic
        self.view.backgroundColor = [UIColor redColor];
    }];
    
    // Register a callback for the down button
    gesture = [MZGesture buttonDownGesture];
    gestures = [NSArray arrayWithObjects: gesture, nil];
    [_mz registerForGestures: gestures  withBlock:^(MZGesture *gesture, Byte *data) {
        // The call back logic
        self.view.backgroundColor = [UIColor greenColor];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
