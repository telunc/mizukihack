//
//  HeadphoneViewController.m
//  Muzik_Hack_Demo
//
//  Created by andy on 2016-04-23.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "HeadphoneViewController.h"
#import "PersonTableViewCell.h"
#import "AddViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface HeadphoneViewController ()

@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UITextField *inputField;
@property (nonatomic, weak) IBOutlet UILabel *battery;
@property (nonatomic, weak) IBOutlet UILabel *btlocalname;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) IBOutlet UIButton *play;
@property (strong, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSTimer *AccelTimer;
@property (strong, nonatomic) NSTimer *CommentTimer;
@property (nonatomic, strong) NSMutableArray *peopleArray;
@property (nonatomic, strong) NSMutableArray *musicArray;
@property (strong, nonatomic) UILabel *comment;
@property (strong, nonatomic) IBOutlet UIView *commentView;

@property (strong, nonatomic) NSURL* Lory;
@property (strong, nonatomic) NSURL* Cam;
@property (strong, nonatomic) NSURL* Justin;
@property (strong, nonatomic) NSURL* OMI;

@end

@implementation HeadphoneViewController

bool isPlay = true;
int trackNumber = 0;

- (IBAction)check{
    
    if (isPlay){
        [_audioPlayer play];
        isPlay = false;
        [self.play setTitle:@"pause" forState:UIControlStateNormal];
        self.slider.maximumValue = self.audioPlayer.duration;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        self.AccelTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(putdown) userInfo:nil repeats:YES];
        self.CommentTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(commentAnimation) userInfo:nil repeats:YES];
    }
    else{
        [_audioPlayer pause];
        isPlay = true;
        [self.play setTitle:@"play" forState:UIControlStateNormal];
        [self.timer invalidate];
        [self.AccelTimer invalidate];
        self.comment.text = @"";
        [self.CommentTimer invalidate];
    }
    
    
//    [self.mz getBatteryLevelWithBlock:^(NSInteger percent) {
//        self.battery.text = [[NSString alloc] initWithFormat:@"%ld", (long)percent];
//        //NSLog(@"%ld", (long) percent);
//    }];
    
}

-(void)playAudio:(int)row{
    
    NSError *error;
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[_musicArray objectAtIndex:row] error:&error];
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@",
              [error localizedDescription]);
    } else {
        [_audioPlayer prepareToPlay];
    }
    
    //reset timer
    [self.timer invalidate];
    [self.AccelTimer invalidate];
    [self.CommentTimer invalidate];
    _comment.text = @"";
    
    //reset slider
    _slider.value = 0;
    
    //play audio and set flag
    [_audioPlayer play];
    isPlay = false;
    
    //chang button text
    [self.play setTitle:@"pause" forState:UIControlStateNormal];
    
    //init time counter
    self.slider.maximumValue = self.audioPlayer.duration;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(putdown) userInfo:nil repeats:YES];
    self.CommentTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(commentAnimation) userInfo:nil repeats:YES];
}


- (IBAction)BTLocalName {
    
    // Get the local blue tooth name
    [self.mz getBluetoothLocalNameWithBlock:^(NSString *name) {
        self.btlocalname.text = name;
    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // navigation bar setting
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:1.0]];
    [[UINavigationBar appearance] setTranslucent:YES];
    [self setTitle:@"Trending"];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil]];
    
    //navigation bar left button
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [backButton setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(add) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    //start headphone server
    self.mz = [[MZAccessory alloc] init];
    
    // Start the server
    [self.mz startServer];
    
    
    // Register for connection state
    [self.mz registerForConnectionState:^(NSUInteger state) {
        switch(state) {
            case 4:
                NSLog(@"headphones ARE connected");
                break;
            case 0:
                NSLog(@"headphones NOT connected");
                break;
            default:
                NSLog(@"something else");
        }
    }];
    
    //[self registerGestures];
    
    //static music
    _musicArray = [[NSMutableArray alloc]init];
    _Lory = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Adele - Hello" ofType:@"mp3"]];
    _Cam = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Cam - Mayday" ofType:@"mp3"]];
    _Justin = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Justin Bieber - Baby" ofType:@"mp3"]];
    _OMI = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"OMI - Cheerleader" ofType:@"mp3"]];
    
    //static array
    self.peopleArray = [[NSMutableArray alloc] init];
    if (self.user.hasLory == true){
        [self.peopleArray addObject:@"Leroy Sanchez" ];
        [self.musicArray addObject:_Lory];
    }
    if (self.user.hasCam == true){
        [self.peopleArray addObject:@"Cam" ];
        [self.musicArray addObject:_Cam];
    }
    if (self.user.hasJustin == true){
        [self.peopleArray addObject:@"Just" ];
        [self.musicArray addObject:_Justin];
    }
    if (self.user.hasOMI == true){
        [self.peopleArray addObject:@"OMI" ];
        [self.musicArray addObject:_OMI];
    }
    
    NSError *error;
    trackNumber = 0;
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[_musicArray objectAtIndex:trackNumber] error:&error];
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@",
              [error localizedDescription]);
    } else {
        [_audioPlayer prepareToPlay];
        
    }
    
    //self.tableView.estimatedRowHeight = 30.0;
    self.tableView.rowHeight = 80.0;
    
    //magic
    [self.tableView registerNib:[UINib nibWithNibName:@"PersonTableViewCell" bundle:nil] forCellReuseIdentifier:@"PersonTableViewCell"];

    
    //self.peopleArray = [[NSMutableArray alloc] initWithObjects: @"Leroy Sanchez", @"Leroy Sanchez", @"Leroy Sanchez", @"Leroy Sanchez", @"Leroy Sanchez", nil];

    //set up comment sliding
    //_comment = [[UILabel alloc] initWithFrame:CGRectMake(300, 21, 124, 21)];
    _comment.textColor = [UIColor whiteColor];
    _commentView.clipsToBounds = true;
    //[self.view addSubview:_commentView];
}

-(void)viewWillDisappear:(BOOL)animated{
    _musicArray = [[NSMutableArray alloc]init];
    self.peopleArray = [[NSMutableArray alloc] init];
    if (self.user.hasLory == true){
        [self.peopleArray addObject:@"Leroy Sanchez" ];
        [self.musicArray addObject:_Lory];
    }
    if (self.user.hasCam == true){
        [self.peopleArray addObject:@"Cam" ];
        [self.musicArray addObject:_Cam];
    }
    if (self.user.hasJustin == true){
        [self.peopleArray addObject:@"Just" ];
        [self.musicArray addObject:_Justin];
    }
    if (self.user.hasOMI == true){
        [self.peopleArray addObject:@"OMI" ];
        [self.musicArray addObject:_OMI];
    }
    [self.tableView reloadData];
}

-(void)add{
    AddViewController *add = [AddViewController alloc];
    add.user = self.user;
//    [self.navigationController presentViewController:[add init] animated:YES completion:nil];
    [self.navigationController pushViewController:[add init] animated:YES];
}

-(void)commentAnimation{
    _comment.text = @"Leroy: Hello~~~!";
    [UIView animateWithDuration:5.0f delay:0.0f options:UIViewAnimationOptionTransitionNone animations:^{
        _comment.frame = CGRectMake(-130, 21, 124, 21);
    } completion:^(BOOL finished) {
        _comment.frame = CGRectMake(300, 21, 124, 21);
    }];
}

- (IBAction)slide {
    _audioPlayer.currentTime = _slider.value;
}

- (void)updateTime:(NSTimer *)timer {
    _slider.value = _audioPlayer.currentTime;
}

-(void)putdown{
    [_mz registerForAccelerometerDataStreamWithBlock:^(float x, float y, float z, float norm, float forwardAngle, float sideAngle) {
        if (forwardAngle >80 || forwardAngle <-80){
            [_audioPlayer pause];
            isPlay = false;
            [self.play setTitle:@"play" forState:UIControlStateNormal];
            [self.timer invalidate];
            [self.AccelTimer invalidate];
        }

    }];
}


- (void)registerGestures{
    
    NSLog(@"%d",trackNumber);
    
    
    // Register a callback for the up button
    MZGesture* gesture = [MZGesture buttonUpGesture];
    NSArray *gestures = [NSArray arrayWithObjects: gesture, nil];
    [_mz registerForGestures: gestures  withBlock:^(MZGesture *gesture, Byte *data) {
        // The call back logic
        if (trackNumber <= _musicArray.count){
            trackNumber++;
            [self playAudio:trackNumber];
        }
        else{
            [self playAudio:0];
        }
    }];
    
    // Register a callback for the down button
    gesture = [MZGesture buttonDownGesture];
    gestures = [NSArray arrayWithObjects: gesture, nil];
    [_mz registerForGestures: gestures  withBlock:^(MZGesture *gesture, Byte *data) {
        // The call back logic
        if (trackNumber-1 <= 0){
            trackNumber--;
            [self playAudio:trackNumber];
        }
        else{
            [self playAudio:(int)_musicArray.count-1];
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.peopleArray.count;
}

- (UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    PersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonTableViewCell" forIndexPath:indexPath];
    cell.name.text = [self.peopleArray objectAtIndex:[indexPath row]];
    NSURL *temp=[self.musicArray objectAtIndex:[indexPath row]];
    cell.title.text = [[[[temp absoluteString] lastPathComponent] stringByReplacingOccurrencesOfString:@"%20" withString:@" "] stringByReplacingOccurrencesOfString:@".mp3" withString:@""];
    NSString *string = @"photo";
    NSString *string2 = [@([indexPath row] +1) stringValue];
    NSLog(@"%@", [NSString stringWithFormat:@"%@%@", string, string2]);
    cell.profileImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", string, string2]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    trackNumber = (int)indexPath.row-1;
    [self playAudio:(int)indexPath.row];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
