//
//  User.h
//  Muzik_Hack_Demo
//
//  Created by andy on 2016-04-24.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (assign) BOOL hasLory;
@property (assign) BOOL hasCam;
@property (assign) BOOL hasJustin;
@property (assign) BOOL hasOMI;

@end
